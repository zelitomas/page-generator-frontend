import { Base64 } from 'js-base64';

const BASE_URI = "https://api.tomaszelina.cz/pdf/";
//const BASE_URI = "http://0.0.0.0:5000/pdf/";

const randomHexId = (len) => {

    let result = "";
    for(let i = 0; i < len; i++) {
        let number = Math.floor(Math.random() * 16);
        result = result + number.toString(16);
    }
    return result.toUpperCase();

};

const getPdfUrl = (template_name, items) => {

    let json = JSON.stringify(items.length === 1 ? items[0] : items);
    let base64 = Base64.encode(json);

    return BASE_URI + template_name + "/" + base64;
};


export {randomHexId, getPdfUrl};
