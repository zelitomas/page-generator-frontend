import React from "react";
import SimpleTable from "./SimpleTable";

class PrintQueue {
    toPrint = [];

    /**
     *
     * @param pdfUrl Promise<String> or String. Should always resolve to and url that can be used by GET and returns valid PDF.
     * @param metadata can only contain 'config' for now :/
     */
    addPdf = (pdfUrl, metadata) => {
        console.log(metadata)
        let pdfUrlPromise = Promise.resolve(pdfUrl);

        let printData = {
            url: pdfUrl,
            metadata: metadata,
            blob: null,
            jobId: Math.floor(Math.random() * 1000000)
        };


        this.toPrint = this.toPrint.concat(printData);
        this.causeUpdate();

        pdfUrlPromise
            .then(pdfUrl => fetch(pdfUrl))
            .then((response) => {
                return response.blob()
            }).then((blob) => {
                printData.blob = blob;
                this.causeUpdate();
                return this.uploadToServer(printData.blob, metadata);
            }).then((response) => {
                printData.finished = true;
                this.causeUpdate()
            }).catch((error) => {
                printData.error = error;
                this.causeUpdate()
            })
    };

    uploadToServer = (blob, metadata) => {
        let form = new FormData();
        form.append("file", blob, "file.pdf");
        if (metadata?.config) {
            form.append("config", metadata?.config);
        }
        console.log(form)
        return fetch(this.getUploadUri(), {
            method: 'POST',
            body: form
        });
    };

    // Do it better
    getUploadUri = () => {
        return this.server + "/print"
    };

    onUpdate = undefined;

    causeUpdate = () => {
        if(this.onUpdate !== undefined){
            this.onUpdate();
        }
    };

    getPrintData = () => {
        return this.toPrint;
    };

    constructor(server) {
        this.server = server;
    }




}


class PrintQueueVisualisation extends React.Component {

    getHumanReadableState = (data) => {
        if(data.error !== undefined) {
            return "Chyba: " + data.error;
        }

        if(data.blob === null) {
            return "Stahování souboru";
        }

        if(data.finished) {
            return "Dokončeno";
        }

        return "Neznámý"
    };

    generateTableData = () => {
        let data = this.props.queue.getPrintData();
        let result = [];
        for(let printData of data){
            result.push({
                jobId: printData.jobId,
                state: this.getHumanReadableState(printData)
            })
        }
        return result;
    };

    render() {
        this.props.queue.onUpdate = () => {this.forceUpdate()};

        if(this.props.hideIfEmpty && this.props.queue.getPrintData().length === 0){
            return null;
        }

        return <>
            <h2>Print log</h2>
            <SimpleTable data={this.generateTableData()} displayedColumns={{jobId: "ID jobu", state: "Stav"}}/>
        </>
    }
}

export {PrintQueue, PrintQueueVisualisation}