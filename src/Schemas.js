let orderLabel = {
    "type": "object",
    "required": [
        "count"
    ],
    "properties": {
        "count": {
            "type": "number",
            "default": 1,
            "title": "Počet štítků"
        },
        "item": {
            "type": "string",
            "title": "Popisek (optional)"
        },

    }
};

let coffeeLegalInfoLabel = {
    "type": "object",
    "required": [
        "copies"
    ],
    "properties": {
        "copies": {
            "type": "number",
            "default": 1,
            "title": "Počet štítků"
        },
    }
};

let grantLabel = {
    "type": "object",
    "required": [
        "grant", "amount"
    ],
    "properties": {
        "grant": {
            "type": "string",
            "title": "Dotační program"
        },
        "amount": {
            "type": "string",
            "title": "Částka"
        },

    }
};

const textLabel = {
    "type": "object",
    "required": [
        "text"
    ],
    "properties": {
        "text": {
            "type": "string",
            "title": "Text"
        },
        "copies": {
            "type": "number",
            "default": 1,
            "title": "Počet štítků"
        },

        "smallText": {
            "type": "boolean",
            "default": false,
            "title": "Menší text"
        },
    }
};

let inverseLabel = window.structuredClone(textLabel)
inverseLabel.properties["inverse"] = {
    "type": "boolean",
    "default": false,
    "title": "Inverzní"
}

const now = new Date();
let defaultExpiry = new Date();
defaultExpiry.setMonth(defaultExpiry.getMonth() + 6);


let coffeeLabel = {
    "type": "object",
    "required": [
        "produced", "expiration", "copies", "weight"
    ],
    "properties": {
        "produced": {
            "type": "string",
            "format": "date",
            "title": "Vyrobeno",
            "default": now.toISOString().slice(0,10)
        },
        "expiration": {
            "type": "string",
            "format": "date",
            "title": "Expirace",
            "default": defaultExpiry.toISOString().slice(0,10)
        },
        "weight": {
            "type": "number",
            "minimum": 0,
            "default": 250,
            "title": "Váha [g]"
        },
        "name": {
            "type": "string",
            "title": "Název (nepovinné)"
        },
        "origin": {
            "type": "string",
            "title": "Původ (nepovinné)"
        },
        "serial": {
            "type": "number",
            "title": "Číslo pražení (nepovinné)"
        },
        "copies": {
            "type": "number",
            "default": 1,
            "title": "Počet štítků"
        },
    }
};

let smallLabel = {
    "type": "object",
    "required": [
        "produced", "expiration", "copies"
    ],
    "properties": {
        "produced": {
            "type": "string",
            "format": "date",
            "title": "Vyrobeno",
            "default": now.toISOString().slice(0,10)
        },
        "expiration": {
            "type": "string",
            "format": "date",
            "title": "Expirace",
            "default": defaultExpiry.toISOString().slice(0,10)
        },
        "copies": {
            "type": "number",
            "default": 1,
            "title": "Počet štítků"
        },
    }
};


const dpp = {
    "type": "object",
    "required": [
        "employee", "trainer", "task", "date"
    ],
    "properties": {
        "task": {
            "type": "string",
            "title": "Typ úkolu",
            "default": "Produkční a technické práce v provozu Industra",
            "enum": ["Produkční a technické práce v provozu Industra", "Umělecký výkon / realizace uměleckého tvaru v provozu Industra"]
        },
        "trainer": {
            "type": "string",
            "title": "Školitel",
            "default": "Šarlota Gunišová",
            "enum": ["Jakub Glöckl", "Šarlota Gunišová", "Pavel Stratil"]
        },
        "employee": {
            "type": "object",
            "title": "Údaje o zaměstnanci",
            "required": ["name", "surname", "address", "dateOfBirth", "placeOfBirth", "healthInsuranceCompany", "familyStatus", "id"],
            "properties": {
                "name": {
                    "type": "string",
                    "title": "Jméno"
                },
                "surname": {
                    "type": "string",
                    "title": "Přijmení"
                },
                "address": {
                    "type": "string",
                    "ui:options": {
                        "widget": "ui:textarea"
                    },
                    "title": "Trvalé bydliště"
                },
                "email": {
                    "type": "string",
                    "format": "email",
                    "title": "E-mail"
                },
                "phone": {
                    "type": "string",
                    "title": "Telefon"
                },
                "dateOfBirth": {
                    "type": "string",
                    "format": "date",
                    "title": "Datum narození",
                },
                "birthNumber": {
                    "type": "string",
                    "pattern": "^[0-9]{6}\\/[0-9]{3,4}$",
                    "title": "Rodné číslo",
                },
                "placeOfBirth": {
                    "type": "string",
                    "title": "Místo narození",
                },
                "healthInsuranceCompany": {
                    "type": "integer",
                    "title": "Zdravotní pojišťovna",
                    "enumNames": ["Česká průmyslová zdravotní pojišťovna (205)", "Oborová zdravotní pojišťovna zaměstnanců bank, pojišťoven a stavebnictví (207)", "RBP, zdravotní pojišťovna (213)", "Všeobecná zdravotní pojišťovna České republiky (111)", "Vojenská zdravotní pojišťovna ČR (201)", "Zaměstnanecká pojišťovna Škoda (209)", "Zdravotní pojišťovna ministerstva vnitra (211)", "Zahraniční zdravotní pojišťovna"],
                    "enum": [205, 207, 213, 111, 201, 209, 211, 0]
                },
                "familyStatus": {
                    "type": "string",
                    "title": "Rodinný stav",
                    "enum": ["Svobodný / svobodná", "Ženatý / vdaný", "Rozvedený / rozvedená", "Ovdovělý / ovdovělá"]
                },
                "bankAccount": {
                    "type": "string",
                    "title": "Číslo účtu",
                    "description": "Na tento účet bude poukazována odměna. Pokud nebude vyplňeno, budeš muset dostat výplatu v hotovosti",
                    "pattern": "^[0-9]{3,20}\\/[0-9]{4}$",

                },
                "id": {
                    "title": "Číslo průkazu totožnosti",
                    "type": "string",
                }

            },


        },
        "date": {
            "type": "string",
            "format": "date",
            "title": "Datum",
            "default": now.toISOString().slice(0,10)
        },

    }
};

export {orderLabel, grantLabel, textLabel, coffeeLabel, dpp, smallLabel, coffeeLegalInfoLabel, inverseLabel};
