import React from 'react';
import {getPdfUrl} from './API';
import Form from "@rjsf/core";
import './App.css';
import {
    CoffeeLabelGenerator,
    DPPGenerator,
    GrantGenerator,
    OrderGenerator,
    SmallLabelGenerator,
    TextGenerator,
    SmallCoffeeLegalInfoGenerator
} from './Generators'
import validator from "@rjsf/validator-ajv6";
import SimpleTable from "./SimpleTable"
import {PrintQueue, PrintQueueVisualisation} from "./DirectPrint";

let REMOTE_SERVER = "https://page-printer.internal.kompresorovna.anilez.cz";

class App extends React.Component {
    configuredGenerators = {
        archiveLabel: {
            displayName: 'Archivační štítek na dokumenty',
            generator: new OrderGenerator("dok", "archive_label")
        },
        archivePage: {
            displayName: 'Archivační stránka na účtenky',
            generator: new OrderGenerator("dok", "archive_page")
        },
        order: {
            displayName: 'Štítek "Scan to order"',
            generator: new OrderGenerator("ord", "order")
        },

        grant: {
            displayName: 'Štítek "Placeno z dotačního programu"',
            generator: new GrantGenerator()
        },

        genericText: {
            displayName: 'Volný štítek',
            generator: new TextGenerator()
        },

        coffee: {
            displayName: 'Štítek na kafe',
            generator: new CoffeeLabelGenerator()
        },
        smallLabel: {
            displayName: 'Malý štítek - exprirace pražené kávy',
            generator: new SmallLabelGenerator()
        },
        coffeeLegalInfo: {
            displayName: 'Malý štítek - právní info pražené kávy',
            generator: new SmallCoffeeLegalInfoGenerator()
        },
        smallGenericText: {
            displayName: 'Malý štítek - volný text',
            generator: new TextGenerator("smallgenerictext", true)
        },
        dpp: {
            displayName: 'Dohoda o provedení práce',
            generator: new DPPGenerator()
        },

    };

    handleCheckboxChange = (event) => {
        this.setState( {
            instaPrint: event.target.checked
        });

    };

    printQueue = undefined;

    constructor() {
        super();

        this.state = {
            generator: "order",
            data: [],
            instaPrint: false
        };

        this.printQueue = new PrintQueue(REMOTE_SERVER);
    }

    resetData = () => {
        this.setState({
            data: []
        })
    };

    getDataToSave = (formData) => {
        return this.getGenerator().transformDataFromForm(formData);
    }

    getMetadata = () => {
        return this.getGenerator().getMetadata === undefined ? {} : this.getGenerator().getMetadata()
    }

    addData = (saveData) => {
        this.setState({
            data: this.state.data.concat(saveData)
        });
    };

    getUrlForData = async (data) => {
        if(data === undefined){
            data = this.state.data;
        }
        const generator = this.getGenerator();
        let dataForPdf = generator.transformSavedData(data);
        if(generator.isAsync) {
            dataForPdf = await dataForPdf
        }
        if(dataForPdf.length === 1) {
            dataForPdf = dataForPdf[0];
        }
        console.log(dataForPdf)

        return getPdfUrl(this.getGenerator().getTemplateName(), dataForPdf);
    };

    getUrlForSingleRecord = (record) => {
        return this.getUrlForData([record]);
    };

    generatePdf = () => {
        this.setState({generating: true});
        this.getUrlForData()
            .then(url => window.open(url))
            .catch(error => {
                console.error(error);
                alert("Generating failed. Please see console for more data.")
            })
            .finally(() => {
                this.setState({generating: false})
            })

    };

    printPdf = (pdfUrl, metadata) => {
        console.log(this);
        console.log(this.printQueue);
        this.printQueue.addPdf(pdfUrl, metadata);
    };

    getGenerator = () => {
        return this.configuredGenerators[this.state.generator].generator;
    };

    generateOptions = () => {
        let result = [];
        for(let generatorName of Object.keys(this.configuredGenerators)){
            result.push(<option value={generatorName}>{this.configuredGenerators[generatorName].displayName}</option>)
        }
        return result;
    };

    changeGenerator = (generatorName) => {
        this.resetData();
        this.setState({
            generator: generatorName
        });
    };

    handleSubmit = async (formData) => {
        let data = this.getDataToSave(formData);
        let metadata = this.getMetadata();

        if(this.state.instaPrint) {
            let pdfUrl = this.getUrlForSingleRecord(data);
            this.printPdf(pdfUrl, metadata);

            // Todo: do something about this...
            this.forceUpdate();
        } else {
            this.addData(data)
        }
    };

    render() {
        //return <h1>Hello</h1>;
        const generator = this.getGenerator();
        let uiSchema = undefined;
        if(generator.getUiSchema !== undefined) {
            uiSchema = generator.getUiSchema()
        }
        return (
            <div id="content">

                <h1>Ruční vygenerování štítku</h1>

                <select
                    value={this.state.generator}
                    onChange={(event) => {this.changeGenerator(event.target.value)}}>
                    {this.generateOptions()}
                </select>

                <div>
                    <input type="checkbox" checked={this.state.instaPrint} onChange={this.handleCheckboxChange}/>
                    Přímý tisk na tiskárně v Industře (experimentální)
                </div>

                {
                    this.state.data.length === 0 ? null :
                        <>
                            <h2>Bude vygenerováno:</h2>
                            <SimpleTable displayedColumns={generator.getDisplayedColumns()} data={this.state.data}/>
                        </>
                }

                {
                this.state.data.length === 0 ? null :
                    <GenerateButtons generating={this.state.generating} onConfirmClick={this.generatePdf} onDeleteClick={this.resetData} />
                }



                <h2>Nový štítek:</h2>
                <Form schema={generator.getSchema()}
                      onSubmit={(data) => {this.handleSubmit(data.formData)}}
                      formData={generator.getInitialData()}
                      uiSchema={uiSchema}
                      validator={validator}
                >
                    <button type="submit">{
                        this.state.instaPrint ? "Tisk" : "Přidat"
                    }
                    </button>
                </Form>

                <PrintQueueVisualisation queue={this.printQueue} hideIfEmpty={true}/>

            </div>


        );
    }
}

function GenerateButtons(props) {
    if(props.generating) {
        return <>Generating...</>
    }
    return <>
        <button className="primary right" onClick={props.onConfirmClick}>Generovat PDF</button>
        <button className="right" onClick={props.onDeleteClick}>Vymazat</button>
        <div className="reset" />
    </>
}


export default App;
