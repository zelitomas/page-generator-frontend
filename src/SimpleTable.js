import React from "react";

class SimpleTable extends React.Component {
    makeRows() {
        let result = [];

        let dataToProcess = this.props.data;

        for (let dataobj of dataToProcess) {
            let rowItems = [];
            for (let allowedColumn of Object.keys(this.props.displayedColumns)) {
                let itemToAdd = "";
                if (dataobj[allowedColumn] !== undefined) {
                    itemToAdd = dataobj[allowedColumn]
                }
                rowItems.push(<td>{itemToAdd}</td>);
            }
            result.push(<tr>{rowItems}</tr>)
        }

        return <tbody>{result}</tbody>
    }

    makeHeader() {
        let headerItems = [];
        for (let h of Object.values(this.props.displayedColumns)) {
            headerItems.push(<th>{h}</th>)
        }

        return <thead>
        <tr>{headerItems}</tr>
        </thead>
    }

    render() {
        return (<table>
            {this.makeHeader()}
            {this.makeRows()}
        </table>);
    }

}

export default SimpleTable;