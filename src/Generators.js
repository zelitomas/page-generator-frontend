/*
 * Todo: define interface and abstract classes
 */

import {
    coffeeLabel,
    coffeeLegalInfoLabel,
    dpp,
    grantLabel,
    inverseLabel,
    orderLabel,
    smallLabel,
    textLabel
} from "./Schemas";
import {getCodes} from "./CodeGenAPI";

let isNumber = (obj) => {
    return !isNaN(obj);
};
/*
class AbstractGenerator {

    getTemplateName = () => "fondy";
    getSchema = () => grantLabel;
    getDisplayedColumns = () => { return {grant: "Dotační program", amount: "Částka"}};
    getInitialData = () => {return {text: this.lastGrantProgram}}
    transformDataFromForm = (formData) => {
        return {
            name: formData.item,
            count: formData.count
        }
    };
    getUiSchema = () => undefined
    transformSavedData = () => {}
}*/

const objectMap = (obj, fn) =>
    Object.fromEntries(
        Object.entries(obj).map(
            ([k, v], i) => [k, fn(k, v, i)]
        )
    );

class CoffeeLabelGenerator {

    getTemplateName = () => "coffeelabel";
    getSchema = () => coffeeLabel;
    getDisplayedColumns = () => objectMap(coffeeLabel.properties, (key, value) => value.title) ;
    getInitialData = () => {return undefined};
    transformDataFromForm = (formData) => formData;
    transformSavedData =  (formData) => formData;
}

class SmallLabelGenerator {

    getTemplateName = () => "smalllabel";
    getSchema = () => smallLabel;
    getDisplayedColumns = () => objectMap(smallLabel.properties, (key, value) => value.title) ;
    getInitialData = () => {return undefined};
    transformDataFromForm = (formData) => formData;
    transformSavedData =  (formData) => formData;
    getMetadata = () => ({
        config: "smalllabel"
    })
}

class DPPGenerator {
    static series = "dok";
    isAsync = true;
    getTemplateName = () => "dpp";
    getSchema = () => dpp;
    getDisplayedColumns = () => ({fullName: "Jméno"}) ;
    getInitialData = () => {return undefined};
    transformDataFromForm = (formData) => {
        return {
            ...formData,
            fullName: formData.employee.name + " " + formData.employee.surname
        }
    };
    transformSavedData = async (collectedData) => {



        const generatedCodes = (await getCodes(DPPGenerator.series, collectedData.length));
        const result = collectedData.map(it => {
            const generatedCode = generatedCodes.pop()
            const codeExtras = getCodeExtras(generatedCode)
            return {
                ...it,
                enddate: new Date((new Date()).getFullYear(), 11, 31).toISOString().slice(0,10),
                "employer": {
                    "company": "Vaizard z. ú.",
                    "address": "Kainarova 26/2672\nBrno Žabovřesky\n616 00",
                    "regNumber": "29228107",
                    "vat": "CZ29228107",
                    "email": "pavel@vaizard.org"
                },

                "reward": {
                    "value": 200,
                    "currency": "CZK"
                },
                doc_url: codeExtras.uri,
                doc_code: codeExtras.code,
                doc_prefix: generatedCode.series.toUpperCase()
            }
        })

        return [result, result].flatMap(x => x);

    };
    getUiSchema = () => ({
        "employee": {
            "address": {
                "ui:widget": "textarea",
                "ui:placeholder": "Příkladová 28/99\nMáslovice\n739 22",
                "ui:rows": 3,
            },
            phone: {
                "ui:inputType": "tel"
            }
        }
    })
}

const passCharacters = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789";

/**
 * Returns calculated params of the code, like password and URL
 * @param code is a code as returned by codegenerator
 */
const getCodeExtras = (code) => {
    const password = [3,4,3].map(it => getPass(it));
    let id = code.code + code.checkSum;
    let prefix = code.series;
    let uri = "https://qr.industra.space/" + prefix.toLowerCase() + "/" + id + "/" + password.join("") ;

    return {
        uri: uri,
        code: id,
        password: password.join("-")
    }
}

const getRandomChar = () => {
    return passCharacters[Math.floor(Math.random() * passCharacters.length)]
}

const getPass = (chars) => {
    let result = "";
    for (let i = 0; i < chars; i++) {
        result = result + getRandomChar()
    }
    return result;
}


class SmallCoffeeLegalInfoGenerator {

    getTemplateName = () => "smalllabelinfo";
    getSchema = () => coffeeLegalInfoLabel;
    getDisplayedColumns = () => objectMap(coffeeLabel.properties, (key, value) => value.title) ;
    getInitialData = () => {return undefined};
    transformDataFromForm = (formData) => formData;
    transformSavedData =  (formData) => formData;
}

class OrderGenerator {
    constructor(series, template, options) {
        this.series = series;
        this.template = template;
        this.options = options ?? {};
    }

    transformDataFromForm = (formData) => {
        return {
            name: formData.item,
            count: formData.count
        }
    };

    isAsync = true;

    transformSavedData = async (collectedData) => {
        let dataForPdf = [];

        const totalCount = collectedData.map(it => (it.count)).reduce((a, b) => (a + b) , 0);
        const codes = await getCodes(this.series, totalCount);

        for (let data of collectedData) {
            for (let i = 0; i < data.count; i++) {
                const c = codes.pop();
                const {password, code, uri} = getCodeExtras(c) ;
                dataForPdf.push({
                    itemname: data.name,
                    prefix: c.series.toUpperCase(),
                    link: uri,
                    code: code,
                    password: password
                })
            }
        }

        return dataForPdf;
    };

    getTemplateName = () => this.template;
    getSchema = () => orderLabel;
    getDisplayedColumns = () => { return {count: "Počet", name: "Popisek"}};
    getInitialData = () => {return undefined}

}

class GrantGenerator {
    lastGrantProgram = "";
    transformDataFromForm = (formData) => {
        this.lastGrantProgram = formData.grant;
        let amount = formData.amount.replace(",", ".");
        if(isNumber(amount)){
            amount = formData.amount + " Kč";
        }
        amount = amount.replace(".", ",");
        return {
            grant: formData.grant,
            amount: amount
        }
    };

    transformSavedData = (collectedData) => {
        return collectedData.map((data) => {
            return {
                grant: data.grant,
                amount: data.amount
            }
        });
    };

    getTemplateName = () => "fondy";
    getSchema = () => grantLabel;
    getDisplayedColumns = () => { return {grant: "Dotační program", amount: "Částka"}};
    getInitialData = () => {return {grant: this.lastGrantProgram}}

}

class TextGenerator {

    constructor(template, supportsInversion) {
        this.supportsInversion = supportsInversion ?? false;
        this.template = template ?? "generictext"
    }

    transformDataFromForm = (formData) => {
        return formData;
    };

    transformSavedData = (collectedData) => {
        return collectedData;
    };

    getTemplateName = () => this.template;
    getSchema = () => this.supportsInversion ? inverseLabel : textLabel;
    getDisplayedColumns = () => { return {text: "Text", copies: "Počet"}};
    getInitialData = () => {return {text: ""}}
}


export {OrderGenerator, GrantGenerator, TextGenerator, CoffeeLabelGenerator, DPPGenerator, SmallLabelGenerator, SmallCoffeeLegalInfoGenerator}
