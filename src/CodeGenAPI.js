

const API_BASE = "https://glued.industra.space/api/codegen/v1/";

const getCodes = async (series, count) => {
    let params = "";
    if(count > 1) {
        params = "?count=" + count
    }
    return await (await fetch(API_BASE + series + params)).json()
};

export {getCodes}